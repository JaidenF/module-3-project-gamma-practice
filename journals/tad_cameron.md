2.13

    Slow going today, but we got the containers working. I was at the helm while everyone else supported.
    We were trying to get mongo working and spin up the containers. Took a while and some SEIR magic, but
    they're up now. The problem ended up being the CRLF end of line sequencer. Whatever that is. All I know is I
    had to set it to LF, I think because I'm windows. Jaiden (also windows) had to do something the same thing. We
    went our separate ways so we can get the reading done.


2.14

    Rough one. We really lack the basic comprehension to do much right now. Spent most of the day struggling
    to make some endpoints, never got very far. We used ChatGPT a lot, asking it to explain some of this code to us. Made a little bit of progress doing that. All in all, some steps forward on general knowledge but
    the project is a little stalled. 
